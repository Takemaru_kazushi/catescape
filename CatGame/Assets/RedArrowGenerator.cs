﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedArrowGenerator : MonoBehaviour
{

    public GameObject RedArrowPrefab;
    float span = 0.1f;
    float delta = 0;


    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(RedArrowPrefab) as GameObject;
            float py = Random.Range(3.55f, -4.6f);
            go.transform.position = new Vector3(11.25f, py, 0);
        }
    }
}
