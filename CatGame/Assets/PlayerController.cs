﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //上矢印が押されたとき
        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0.1f, 0); 
        }
          
            //下矢印が押されたとき
            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(0, -0.1f, 0); 
            }

    }
}
